<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the annobin plugin on the Fedora system.

Copyright © 2018 - 2024 Red Hat.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Hardened Command Line Options (Annobin)</title>

<meta name="description" content="Hardened Command Line Options (Annobin)">
<meta name="keywords" content="Hardened Command Line Options (Annobin)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Hardened.html" rel="up" title="Hardened">
<link href="Waiving-Hardened-Results.html" rel="next" title="Waiving Hardened Results">
<link href="The-tests.html" rel="prev" title="The tests">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Hardened-Command-Line-Options">
<div class="nav-panel">
<p>
Next: <a href="Waiving-Hardened-Results.html" accesskey="n" rel="next">How to waive the results of the hardening tests</a>, Previous: <a href="The-tests.html" accesskey="p" rel="prev">The Tests Run By Annocheck</a>, Up: <a href="Hardened.html" accesskey="u" rel="up">The Hardened security checker.</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Command-line-options-specific-to-the-hardened-tool"><span>4.2.2 Command line options specific to the hardened tool<a class="copiable-link" href="#Command-line-options-specific-to-the-hardened-tool"> &para;</a></span></h4>

<dl class="table">
<dt><code class="code">--skip-<var class="var">test</var>[=<var class="var">funcname</var>]</code></dt>
<dd><p>Disable the test called <var class="var">test</var>.  If the optional <var class="var">funcname</var>
argument is supplied then the test is only disabled for the named
function (and by implication it is enabled for other functions).  This
extended version of the option can be used multiple times to allow the
test to be skipped for multiple functions.
</p>
</dd>
<dt><code class="code">--skip-all</code></dt>
<dd><p>Disable all tests.  Not really useful unless followed by one or more
options to enable specific tests.
</p>
<p>Note - using this option also sets the <samp class="option">profile</samp> to
<em class="emph">none</em>.  If the enabling of profile specific tests is desired the
<samp class="option">--profile</samp> option must appear after the <samp class="option">--skip-all</samp> on
the command line.
</p>
</dd>
<dt><code class="code">--test-<var class="var">name</var></code></dt>
<dd><p>Enable test <var class="var">name</var>.
</p>
</dd>
<dt><code class="code">--test-all</code></dt>
<dd><p>Enable all the tests.
</p>
</dd>
<dt><code class="code">--test-future</code></dt>
<dt><code class="code">--skip-future</code></dt>
<dt><code class="code">--enable-future</code></dt>
<dt><code class="code">--disable-future</code></dt>
<dd><p>Enable all the <em class="emph">future fail</em> tests.  These are tests for security
features which are not yet implemented or widely adopted, but which
are planned for the future.  The <samp class="option">--skip-future</samp> option can be
used to restore the default behaviour of skipping all of these tests.
</p>
<p>Individual future fail tests can be enabled by name by using
<samp class="option">--test-<var class="var">name</var></samp>.  In addition it is possible to allow
future fails without specifically enabling any of them by using the
<samp class="option">--enable-future</samp> option, and to disable them by using the
<samp class="option">--disable-future</samp> option.  This may be useful as some tests
have parts which are future fail and parts which are not.
</p>
</dd>
<dt><code class="code">--test-unicode-all</code></dt>
<dt><code class="code">--test-unicode-suspicious</code></dt>
<dd><p>The <samp class="option">--test-unicode</samp> test checks for the presence of multibyte
characters in symbol names, which are unusual and potentially
dangerous.  The test has two modes of operation.  In one mode, enabled
by <samp class="option">--test-unicode-all</samp>, any multibyte character is considered
suspicious.  This mode is good for code bases where multibyte
characters are not expected to appear at all.
</p>
<p>In the other mode, enabled by <samp class="option">--test-unicode-suspicious</samp>, only
potentially dangerous unicode characters trigger a failure.  See
<a class="ref" href="Test-unicode.html">The unicode test</a> for more details on which characters are considered
suspicious.
</p>
<p>If neither of these options is specified, the default depends upon the
profile selected.  If a profile is not selected then the default is
only fail upon the detection of suspicious characters.
</p>
</dd>
<dt><code class="code">--skip-passes</code></dt>
<dt><code class="code">--no-skip-passes</code></dt>
<dd><p>Tells annocheck to display (or not display) a &ldquo;PASS &lt;filename&gt;&rdquo;
result for each binary that passes all of the tests.  In addition, if
running in <samp class="option">--verbose</samp> mode, individual tests that succeed will
be displayed as &ldquo;&lt;filename&gt; PASS &lt;testname&gt;&rdquo; if this option is
enabled.  The default is to have the option enabled.
</p>
</dd>
<dt><code class="code">--profile=el7</code></dt>
<dt><code class="code">--profile=rhel-7</code></dt>
<dt><code class="code">--profile=el8</code></dt>
<dt><code class="code">--profile=rhel-8</code></dt>
<dt><code class="code">--profile=el9</code></dt>
<dt><code class="code">--profile=rhel-9</code></dt>
<dt><code class="code">--profile=el10</code></dt>
<dt><code class="code">--profile=rhel-10</code></dt>
<dt><code class="code">--profile=rawhide</code></dt>
<dt><code class="code">--profile=f43</code></dt>
<dt><code class="code">--profile=f42</code></dt>
<dt><code class="code">--profile=f41</code></dt>
<dt><code class="code">--profile=f40</code></dt>
<dt><code class="code">--profile=f39</code></dt>
<dt><code class="code">--profile=f38</code></dt>
<dt><code class="code">--profile=f37</code></dt>
<dt><code class="code">--profile=f36</code></dt>
<dt><code class="code">--profile=f35</code></dt>
<dt><code class="code">--profile=rhivos</code></dt>
<dt><code class="code">--profile=default</code></dt>
<dt><code class="code">--profile=none</code></dt>
<dt><code class="code">--profile=auto</code></dt>
<dd><p>Rather than enabling and disabling specific tests a selection can be
chosen via a profile option.  The <samp class="option">--profile=el7</samp> and
<samp class="option">--profile=rhel-7</samp> options will select the tests suitable for
<em class="emph">RHEL-7</em> binaries.  Similarly <samp class="option">--profile=el8</samp> or
<samp class="option">--profile=rhel-8</samp> configures the tests for <em class="emph">RHEL-8</em> and 
so on.
</p>
<p>The  <samp class="option">--profile=rawhide</samp> option will select tests suitable for
<em class="emph">Fedora rawhide</em> binaries, whilst <samp class="option">--profile=f38</samp> selects
tests suitable for <em class="emph">Fedora F38</em>, and so on for the other Fedora
releases.
</p>
<p>Other profiles may be added in the future.
</p>
<p>The <samp class="option">--profile=rhivos</samp> option enables tests mandated for
RHIVOS development.
</p>
<p>The <samp class="option">--profile=auto</samp> option will attempt to determine the
profile to use, based upon the input filename.  This only works with
<samp class="file">rpms</samp>, which include the OS as part of their name.  This option
is the default.  The <samp class="option">--profile=default</samp> option is a synonym
for the <samp class="option">--profile=auto</samp> option.
</p>
<p>Using <samp class="option">--profile=none</samp> will disable the profiling.
</p>
<p>For backwards compatibility the form <samp class="option">--profile-&lt;name&gt;</samp> can be
used instead of <samp class="option">--profile=&lt;name&gt;</samp>.
</p>
<p>Currently the profiles enable and disable the following tests:
</p>
<dl class="table">
<dt><code class="code">el9</code></dt>
<dt><code class="code">f35</code></dt>
<dd><p>Disables the <a class="ref" href="Test-branch-protection.html">The branch-protection test</a> and <a class="ref" href="Test-dynamic-tags.html">The dynamic-tags test</a>
tests and enables their inverse, ie <a class="ref" href="Test-not-branch-protection.html">The not-branch-protection test</a>
and <a class="ref" href="Test-not-dynamic-tags.html">The not-dynamic-tags test</a>.
</p>
<p>Also enables <a class="ref" href="Test-unicode.html">The unicode test</a> and sets the default to fail for any
multibyte character.
</p>
</dd>
<dt><code class="code">el8</code></dt>
<dd><p>Like <code class="code">el9</code> but also disables the <a class="ref" href="Test-lto.html">The lto test</a> test.
</p>
</dd>
<dt><code class="code">el7</code></dt>
<dd><p>Like <code class="code">el8</code> but also disables the <a class="ref" href="Test-pie.html">The pie test</a>, <a class="ref" href="Test-bind-now.html">The bind-now test</a>, <a class="ref" href="Test-fortify.html">The fortify test</a> and <a class="ref" href="Test-stack-clash.html">The stack-clash test</a> tests.
</p>
</dd>
<dt><code class="code">el10</code></dt>
<dd><p>Enables the <a class="ref" href="Test-branch-protection.html">The branch-protection test</a> and <a class="ref" href="Test-dynamic-tags.html">The dynamic-tags test</a>
tests and disables their inverse, ie <a class="ref" href="Test-not-branch-protection.html">The not-branch-protection test</a>
and <a class="ref" href="Test-not-dynamic-tags.html">The not-dynamic-tags test</a>.
</p>
</dd>
<dt><code class="code">rawhide</code></dt>
<dt><code class="code">f36</code></dt>
<dd><p>Like <code class="code">el10</code> but also disables the See <a class="xref" href="Test-fips.html">The FIPS test</a> test.
</p>
</dd>
</dl>

<p>In addition the <a class="ref" href="Test-unicode.html">The unicode test</a> test is enabled for all of the RHEL
profiles, but disabled for the Fedora profiles.
</p>
</dd>
<dt><code class="code">--disable-hardened</code></dt>
<dt><code class="code">--enable-hardened</code></dt>
<dd><p>Disables or enables the tool.
</p>
</dd>
<dt><code class="code">--ignore-gaps</code></dt>
<dt><code class="code">--report-gaps</code></dt>
<dd><p>The <samp class="option">--ignore-gaps</samp> option is an alias for the
<samp class="option">--skip-gaps</samp> option which disables the gaps test.
The <samp class="option">--report-gaps</samp> option is an alias for the
<samp class="option">--test-gaps</samp> option which enables the gaps test if
it has been disabled.  (The test is enabled by default).
</p>
</dd>
<dt><code class="code">--fixed-format-messages</code></dt>
<dd><p>Display messages in a fixed, machine parseable format.  The format is:
</p>
<div class="example smallexample">
<pre class="example-preformatted">Hardened: &lt;result&gt;: test: &lt;test-name&gt; file: &lt;file-name&gt;
</pre></div>

<p>Where <code class="code">&lt;result&gt;</code> is <em class="emph">PASS</em> or <em class="emph">FAIL</em> and
<code class="code">&lt;test-name&gt;</code> is the name of the test, which is the same as the
name used in the <samp class="option">--test-&lt;test-name&gt;</samp> option.  The
<code class="code">&lt;filename&gt;</code> is the name of the input file, but with any special
characters replaced so that it always fits on one line.
</p>
<p>Here is an example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">  Hardened: FAIL: test: pie file: a.out.
</pre></div>

</dd>
<dt><code class="code">--disable-colour</code></dt>
<dt><code class="code">--enable-colour</code></dt>
<dt><code class="code">--disable-color</code></dt>
<dt><code class="code">--enable-color</code></dt>
<dd><p>Do not use colour to enhance FAIL, MAYB and WARN messages.  By default
annocheck will add colour to these messages so that they stand out
when displayed by a terminal emulator.  This option can be used in
order to turn this feature off.  The feature can be re-enabled with
<samp class="option">--enable-colour</samp>.  The American spelling of color is also
supported.
</p>
</dd>
<dt><code class="code">--show-totals</code></dt>
<dt><code class="code">--no-show-totals</code></dt>
<dd><p>Do, or do not, show cumulative results when testing multiple files.
The default is to show the cumulative results.
</p>
</dd>
<dt><code class="code">--allow-exceptions</code></dt>
<dt><code class="code">--no-allow-exceptions</code></dt>
<dd><p>By default annocheck will skip some tests for known special case
binaries - eg glibc components or gcc support files.  This behaviour
can be disabled by the <samp class="option">--no-allow-exceptions</samp> option and
restored by the <samp class="option">--allow-exceptions</samp>.
</p>
</dd>
<dt><code class="code">--full-filenames</code></dt>
<dt><code class="code">--base-filenames</code></dt>
<dd><p>Use the full pathname for files.  Useful when recursing into
directories.   By default this feature is disabled in normal mode and
enabled in <samp class="option">verbose</samp> mode.  This option and its inverse
<samp class="option">--base-filenames</samp> can be used to set a fixed choice.
</p>
</dd>
<dt><code class="code">--suppress-version-warnings</code></dt>
<dd><p>Do not issue warning messages about version mismatches between the
version of the compiler used to build the annobin plugin and the
version of the compiler used to run the annobin plugin.
</p>
</dd>
<dt><code class="code">--no-urls</code></dt>
<dt><code class="code">--provide-urls</code></dt>
<dd><p>By default when a FAIL or MAYB result is displayed by the
<var class="var">hardened</var> checker and <samp class="option">--verbose</samp> is enabled, a URL to the
online version of the relevant section in this document is also
displayed.  (Unless the <samp class="option">--fixed-format-messages</samp> option has
been enabled).  The <samp class="option">--no-urls</samp> option disables the display of
the URLs and the <samp class="option">--provide-urls</samp> re-enables the display (even
in non-verbose mode).
</p>
</dd>
</dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Waiving-Hardened-Results.html">How to waive the results of the hardening tests</a>, Previous: <a href="The-tests.html">The Tests Run By Annocheck</a>, Up: <a href="Hardened.html">The Hardened security checker.</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
