<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the annobin plugin on the Fedora system.

Copyright © 2018 - 2024 Red Hat.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Absence of compiled code (Annobin)</title>

<meta name="description" content="Absence of compiled code (Annobin)">
<meta name="keywords" content="Absence of compiled code (Annobin)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Hardened.html" rel="up" title="Hardened">
<link href="Waiving-Hardened-Results.html" rel="prev" title="Waiving Hardened Results">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Absence-of-compiled-code">
<div class="nav-panel">
<p>
Previous: <a href="Waiving-Hardened-Results.html" accesskey="p" rel="prev">How to waive the results of the hardening tests</a>, Up: <a href="Hardened.html" accesskey="u" rel="up">The Hardened security checker.</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<h4 class="subsection" id="What-to-do-if-annocheck-reports-that-it-could-not-find-compiled-code_002e"><span>4.2.4 What to do if annocheck reports that it could not find compiled code.<a class="copiable-link" href="#What-to-do-if-annocheck-reports-that-it-could-not-find-compiled-code_002e"> &para;</a></span></h4>

<p>The hardening checker will skip or fail some tests if it cannot
prove that the file being checked was created by a known compiler, or
if the code was created from assembler sources, rather than a high
level language.  This is because the test is checking
for a specific feature of a specific compiler.
</p>
<p>The checker uses several different methods for determining if an
executable was compiled:
</p>
<dl class="table">
<dt><code class="code">notes</code></dt>
<dd><p>If there are annobin notes present, these include a description of the
compiler used to create the executable.
</p>
</dd>
<dt><code class="code">DWARF</code></dt>
<dd><p>If DWARF debug information is available, the compiler can usually be
found in the DW_AT_producer attribute and the source language in the
DW_AT_language attribute.
</p>
</dd>
<dt><code class="code">comment</code></dt>
<dd><p>If there is a <em class="emph">.comment</em> section in the file, then this usually
contains the name of the compiler.
</p>
</dd>
<dt><code class="code">GO note</code></dt>
<dd><p>The presence of a <em class="emph">.note.go.buildid</em> section indicates that the
file contains GO compiler code.
</p>
</dd>
<dt><code class="code">GO symbol</code></dt>
<dd><p>The presence of a variable called &lsquo;<samp class="samp">go1.&lt;V&gt;</samp>&rsquo; in the read-only data
section.  Again this indicates the presence of GO compiled code.
</p>
</dd>
<dt><code class="code">executable segments</code></dt>
<dd><p>If the file contains one or more program segments with the
<em class="emph">executable</em> flag set, then this indicates that it is likely to
contain compiled code.
</p>
</dd>
</dl>

<p>There are several reasons why annocheck might think that the file does
not originate from compiled source code:
</p>
<dl class="table">
<dt><code class="code">fake assembler</code></dt>
<dd><p>Sometimes when compiling code it is desireable to be able to build it
without certain security options, but also without annocheck
complaining about them.  This is used by <code class="command">glibc</code> for example
because it does not use stack checking or function fortification.
</p>
<p>This effect can be achieved with <code class="command">GCC</code> by using the
<samp class="option">-Wa,--generate-missing-build-notes</samp> command line option.  This
tells the assembler to generate a fake annobin note that tells
annocheck to treat all of the code as if it has been produced from
assembler sources <em class="emph">and</em> to ignore tests specific to high level
languages. 
</p>
</dd>
<dt><code class="code">real assembler</code></dt>
<dd><p>The file was created from assembler source code or some other low
level language.  In this case futher manual checking is warranted.  If
the source code has not been written with the particular security
feature in mind, then it may be vulnerable.
</p>
<p>If on the other hand it does not need the security feature or it has
been written to support it, then adding an annobin note to the
assembler sources will stop annocheck from complaining.
</p>
<p>For example the following will add a note about strong stack
protection:
</p>
<div class="example smallexample">
<pre class="example-preformatted">	.pushsection .gnu.build.attributes, &quot;o&quot;, %note, .text
	.balign 4
	.dc.l 2f - 1f	# Size of the name field.
	.dc.l 0		# Empty description field.
	.dc.l 0x100	# This is an OPEN note which applies to all the code in the covered region.
      1:
	.dc.b 'G', 'A', # This is a GNU attribute
        .dc.b '*'       # It contains a numeric value
        .dc.b 0x2       # For the -fstack-protector option
        .dc.b 0x3       # The value is 3, which indicated -fstack-protector-strong
        .dc.b 0         # Since this field is nominally a name, it ends with a NUL byte.
      2:
	.dc.b 0, 0 	# Padding to ensure note ends on a 4 byte boundary.
	.popsection
</pre></div>

<p>Compiling a simple C program with the <samp class="option">-S -fverbose-asm
-fplugin=annobin &lt;security option&gt;</samp> options should provide an example
of how to encode a note about <em class="emph">&lt;security option&gt;</em>.
</p>
<p>Note - in order for annobin notes to work at least one of them needs
to specify the address range that they cover.  This is usually done by
a version note, which details the version number of the tool used to
produce the code.  For assembler this note would look like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">	.pushsection .gnu.build.attributes, &quot;o&quot;, %note, .text
	.balign 4
	.dc.l 2f - 1f   # Size of the name field.
	.dc.l 16	# Size of the description field (= 2 * sizeof (address)).
	.dc.l 0x100	# This is an OPEN note which applies to all the code in the covered region.
      1:
	.dc.b 'G', 'A', # This is a GNU Attribute note.
        .dc.b '$'       # It contains a string value.
        .dc.b 0x1       # The string is a VERSION string.
        .dc.b '3',      # Version 3 of the Watermark Protocol is being used.
        .dc.b 'a',      # The code has been produced by an 'a'ssembler.
        .dc.b '2'       # The assembler's major version number is 2.
        .dc.b 0         # Since this field is nominally a name, it ends with a NUL byte.
      2:
                        # Coincidentally, no padding is needed here.
        .quad &lt;insert-start-symbol-here&gt;
	.quad &lt;insert-end-symbol-here&gt;
	.popsection
</pre></div>

</dd>
<dt><code class="code">unsupported source language</code></dt>
<dd><p>The file was created by compiling high-level language source code, but
in a language with which annocheck is unfamiliar.  In this case it may
still make sense to skip the test, if it is checking for a feature
that is not supported by the language&rsquo;s compiler.
</p>
</dd>
<dt><code class="code">annobin annotation not enabled</code></dt>
<dd><p>The file was created by compiling C and/or C++ but without any annobin
notation enabled, and without any debug information generation.  In
this case there may be a problem, since the test being skipped might
actually fail if annocheck knew that the file was compiled.
</p>
<p>Fixing this problem involves investigating how the executable was
built and adding the necessary rules to invoke the annobin plugin and
the sought after security hardening options.
</p>
</dd>
<dt><code class="code">stripped of symbols and notes</code></dt>
<dd><p>The file was built normally, but then stripped of most of the useful
information, including its symbol table, debug information, annobin
notes and so on.
</p>
<p>Restoring the stripped information should solve this problem.
</p>
</dd>
<dt><code class="code">missing debug information file</code></dt>
<dd><p>Commonly binaries that are compiled with debug information enabled
have that information moved into a separate debug info file as part of
the build process.  If annocheck is unable to locate the debug info
file that is associated with a binary then it looses out on a lot of
information.
</p>
<p>In addition it is quite common for the build process to also move any
annobin notes into the separate debug info file.  So again if that
file cannot be found then annocheck is hampered in its activities.
</p>
</dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Waiving-Hardened-Results.html">How to waive the results of the hardening tests</a>, Up: <a href="Hardened.html">The Hardened security checker.</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
