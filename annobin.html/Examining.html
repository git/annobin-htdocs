<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the annobin plugin on the Fedora system.

Copyright © 2018 - 2024 Red Hat.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Examining (Annobin)</title>

<meta name="description" content="Examining (Annobin)">
<meta name="keywords" content="Examining (Annobin)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="Annocheck.html" rel="next" title="Annocheck">
<link href="Plugins.html" rel="prev" title="Plugins">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="chapter-level-extent" id="Examining">
<div class="nav-panel">
<p>
Next: <a href="Annocheck.html" accesskey="n" rel="next">Analysing binary files.</a>, Previous: <a href="Plugins.html" accesskey="p" rel="prev">How to add Binary Annotations to your application.</a>, Up: <a href="index.html" accesskey="u" rel="up">Annotating Binaries: How Was Your Program Built ?</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<h2 class="chapter" id="How-to-examine-the-information-stored-in-the-binary_002e"><span>3 How to examine the information stored in the binary.<a class="copiable-link" href="#How-to-examine-the-information-stored-in-the-binary_002e"> &para;</a></span></h2>


<p>The information is stored in a binary in either the ELF Note format
inside a special section called <code class="code">.gnu.build.attributes</code>, or else
as ordinary strings inside a section called <code class="code">.annobin.notes</code>.
</p>
<p>The <code class="code">readelf</code> program from the <code class="code">binutils</code> package can
extract and display these notes.  (Adding the <samp class="option">--wide</samp>
option is also helpful).
</p>
<p>If the information is held in the ELF note format then readelf&rsquo;s
<samp class="option">--notes</samp> option will display them.  Here is an example of the
output:
</p>
<div class="example smallexample">
<pre class="example-preformatted">Displaying notes found in: .gnu.build.attributes
  Owner                        Data size	Description
  GA$&lt;version&gt;3p3              0x00000010	OPEN	    Applies to region from 0x8a0 to 0x8c6 (hello.c)
  GA$&lt;tool&gt;gcc 7.2.1 20170915  0x00000000	OPEN	    Applies to region from 0x8a0 to 0x8c6
  GA*GOW:0x452b                0x00000000	OPEN	    Applies to region from 0x8a0 to 0x8c6
  GA*&lt;stack prot&gt;strong        0x00000000	OPEN	    Applies to region from 0x8a0 to 0x8c6
  GA*GOW:0x412b                0x00000010	func	    Applies to region from 0x8c0 to 0x8c6 (baz)
</pre></div>

<p>This shows various different pieces of information, including the fact
that the notes were produced using version 3 of the specification, and
version 3 of the plugin.  The binary was built by gcc version 7.2.1
and the -fstack-protector-strong option was enabled on the command
line.  The program was compiled with -O2 enabled except the baz()
function which was compiled with -O0 instead.
</p>
<p>The most complicated part of the notes is the owner field.  This is
used to encode the type of note as well as its value and possibly
extra data as well.  The format of the field is explained in detail in
the Watermark specification, but it basically consists of the letters
&lsquo;<samp class="samp">G</samp>&rsquo; and &lsquo;<samp class="samp">A</samp>&rsquo; followed by an encoding character (one of
&lsquo;<samp class="samp">*$!+</samp>&rsquo;) and then a type character and finally the value.
</p>
<p>The notes are always four byte aligned, even on 64-bit systems.  This
does mean that consumers of the notes may have to read 8-byte wide
values from 4-byte aligned addresses, and that producers of the
notes may have to generate unaligned relocs when creating them.
</p>
<p>If the information is held as strings then readelf&rsquo;s
<samp class="option">-p.annobin.notes</samp> option will display them.  Here is an
example of the output:
</p>
<div class="example smallexample">
<pre class="example-preformatted">String dump of section '.annobin.notes':
  [     0]  AV:4.p.1200
  [     c]  RV:running gcc 12.2.1 20221121
  [    2b]  BV:annobin gcc 12.2.1 20221121
  [    4a]  PN:annobin
  [    55]  GW:0x290540
  [    61]  SP:3
</pre></div>


<p>Most of the notes have a reasonably self explanatory name and value.
The exception are the <code class="code">version</code> and <code class="code">GOW</code> notes, which are
included in the table below.
</p>

<ul class="mini-toc">
<li><a href="The-Version-Encoding.html" accesskey="1">Encoding Protocol and Producer Versions</a></li>
<li><a href="The-STACK-Encoding.html" accesskey="2">Encoding Stack Protections</a></li>
<li><a href="The-PIC-Encoding.html" accesskey="3">Encoding Position Independence</a></li>
<li><a href="The-GOW-Encoding.html" accesskey="4">Encoding Optimization and Debugging Levels</a></li>
<li><a href="The-CF-Encoding.html" accesskey="5">Encoding Control Flow Protection</a></li>
<li><a href="The-ENUM-Encoding.html" accesskey="6">Encoding the Size of Enumerations</a></li>
<li><a href="The-INSTRUMENT-Encoding.html" accesskey="7">Encoding Instrumentation Options</a></li>
<li><a href="String-Format-Notes.html" accesskey="8">Encoding Notes in a string format</a></li>
</ul>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Annocheck.html">Analysing binary files.</a>, Previous: <a href="Plugins.html">How to add Binary Annotations to your application.</a>, Up: <a href="index.html">Annotating Binaries: How Was Your Program Built ?</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
