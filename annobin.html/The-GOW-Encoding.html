<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the annobin plugin on the Fedora system.

Copyright © 2018 - 2024 Red Hat.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>The GOW Encoding (Annobin)</title>

<meta name="description" content="The GOW Encoding (Annobin)">
<meta name="keywords" content="The GOW Encoding (Annobin)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Examining.html" rel="up" title="Examining">
<link href="The-CF-Encoding.html" rel="next" title="The CF Encoding">
<link href="The-PIC-Encoding.html" rel="prev" title="The PIC Encoding">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="The-GOW-Encoding">
<div class="nav-panel">
<p>
Next: <a href="The-CF-Encoding.html" accesskey="n" rel="next">Encoding Control Flow Protection</a>, Previous: <a href="The-PIC-Encoding.html" accesskey="p" rel="prev">Encoding Position Independence</a>, Up: <a href="Examining.html" accesskey="u" rel="up">How to examine the information stored in the binary.</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<h3 class="section" id="Encoding-Optimization-and-Debugging-Levels"><span>3.4 Encoding Optimization and Debugging Levels<a class="copiable-link" href="#Encoding-Optimization-and-Debugging-Levels"> &para;</a></span></h3>

<p>The <code class="code">GOW</code> note encodes the optimization level (<samp class="option">-O</samp>) and
debugging level (<samp class="option">-g</samp>) used when compiling a binary.  In order
to save space this is stored as a bit field with the bits having the
following meanings:
</p>
<dl class="table">
<dt><code class="code">bits 0 - 2</code></dt>
<dd><p>The debug type, ie DBX, DWARF, VMS or XCOFF.  As specified by the
<samp class="option">-gstabs</samp>, <samp class="option">-gdwarf</samp>, <samp class="option">-gvms</samp> and
<samp class="option">-gxcoff</samp> options.
</p>
</dd>
<dt><code class="code">bit  3</code></dt>
<dd><p>Set if GNU extensions to the debug type have been enabled.
</p>
</dd>
<dt><code class="code">bits 4 - 5</code></dt>
<dd><p>The debug info level ie TERSE, NORMAL or VERBOSE as set by the
<samp class="option">-g&lt;level&gt;</samp> option.
</p>
</dd>
<dt><code class="code">bits 6 -  8</code></dt>
<dd><p>The DWARF version, if DWARF is being generated.  Set by the
<samp class="option">-gdwarf-&lt;version&gt;</samp> option.
</p>
</dd>
<dt><code class="code">bits 9 - 10</code></dt>
<dd><p>The optimization level as set by the <samp class="option">-O&lt;number&gt;</samp> option.
Levels above 3 are treated as if they were 3.
</p>
</dd>
<dt><code class="code">bit 11</code></dt>
<dd><p>Set if the optimize-for-size option (<samp class="option">-Os</samp>) is enabled.
</p>
</dd>
<dt><code class="code">bit 12</code></dt>
<dd><p>Set if the inaccurate-but-fast optimization option (<samp class="option">-Ofast</samp>)
has been enabled.
</p>
</dd>
<dt><code class="code">bit 13</code></dt>
<dd><p>Set if the optimize-with-debugging option (<samp class="option">-Og</samp>) has been
enabled.
</p>
</dd>
<dt><code class="code">bit 14</code></dt>
<dd><p>Set if the enable most warnings option (<samp class="option">-Wall</samp>) has been
enabled.
</p>
</dd>
<dt><code class="code">bit 15</code></dt>
<dd><p>Set if the format security warning option (<samp class="option">-Wformat-security</samp>)
has been enabled.
</p>
</dd>
<dt><code class="code">bit 16</code></dt>
<dd><p>Set if LTO compilation has been enabled.
</p>
</dd>
<dt><code class="code">bit 17</code></dt>
<dd><p>Set if LTO compilation has not been enabled.
</p>
<p>This bit is here so that tools can detect notes created by earlier
versions of annobin which did not set any bits higher than 15.
</p>
</dd>
<dt><code class="code">bits 18 - 19</code></dt>
<dd><p>These bits are used to record the setting of gcc&rsquo;s
<samp class="option">-ftrivial-auto-var-init</samp> command line option.  If both bits
are clear then the option is not supported by the compiler.
If bit 18 is set but bit 19 is clear, then the compiler supports the
option, but either it was not used, or it was used but the option&rsquo;s
value was <samp class="option">skip</samp>.  Otherwise if bit 19 is clear then the option
was used but its value was set to <samp class="option">pattern</samp> (which is
inappropriate for production binaries) or if bit 19 is set then the
option was used and its value was set to <samp class="option">zero</samp>.
</p>
</dd>
<dt><code class="code">bits 20 - 21</code></dt>
<dd><p>These bits are used to record the setting of gcc&rsquo;s
<samp class="option">-fzero-call-used-regs</samp> command line option.  If both bits are
clear the option is not supported by the compiler.  Otherwise if bit
20 is set (and bit 21 is clear) then the option is supported but it
was not used, or it was used, but its value was set to <samp class="option">skip</samp>.
Otherwise both bits 20 and 21 should be set, indicating that the
option was used and a value other than <samp class="option">skip</samp> was used.
</p>
</dd>
<dt><code class="code">bits 22 - 23</code></dt>
<dd><p>These bits are used to record the setting of gcc&rsquo;s
<samp class="option">-Wimplicit-int</samp> warning.  Enabling this warning is important
as variables with an implicit type of int can cause problems when they
are used in situations where they are expected to be compatible with
pointers.  The meanings of the two bits are as follows:
</p>
<dl class="table">
<dt><code class="code">bit22 = 0, bit23 = 0</code></dt>
<dd><p>The annobin plugin did not record any information about this warning,
probably because the plugin is an old version.
</p></dd>
<dt><code class="code">bit22 = 1, bit23 = 0</code></dt>
<dd><p>The warning has been disabled.
</p></dd>
<dt><code class="code">bit22 = 0, bit23 = 1</code></dt>
<dd><p>The warning has its default value.  It has not been altered via a
command line option.
</p></dd>
<dt><code class="code">bit22 = 1, bit23 = 1</code></dt>
<dd><p>The warning has been enabled via a command line option.
</p></dd>
</dl>

<p>Note - whilst this warning is normally enabled it can be manually
disabled.  Hence the need for this extra test.
</p>
<p>Note - this test is only really relevent to C source code.  Other
languages are not affected.
</p>
</dd>
<dt><code class="code">bits 24 - 25</code></dt>
<dd><p>These bits are used to record the setting of gcc&rsquo;s
<samp class="option">-Wimplicit-function-declaration</samp> warning.  Enabling this
warning is important as functions that are assumed to accept int
parameters or return int values can cause problems when pointers are
involved.  The meanings of the two bits are as follows:
</p>
<dl class="table">
<dt><code class="code">bit22 = 0, bit23 = 0</code></dt>
<dd><p>The annobin plugin did not record any information about this warning,
probably because the plugin is an old version.
</p></dd>
<dt><code class="code">bit22 = 1, bit23 = 0</code></dt>
<dd><p>The warning has been disabled.
</p></dd>
<dt><code class="code">bit22 = 0, bit23 = 1</code></dt>
<dd><p>The warning has its default value.  It has not been altered via a
command line option.
</p></dd>
<dt><code class="code">bit22 = 1, bit23 = 1</code></dt>
<dd><p>The warning has been enabled via a command line option.
</p></dd>
</dl>

<p>Note - whilst this warning is normally enabled it can be manually
disabled.  Hence the need for this extra test.
</p>
<p>Note - this test is only really relevent to C source code.  Other
languages are not affected.
</p>
</dd>
<dt><code class="code">bits 26 - 28</code></dt>
<dd><p>These bits record the settings of gcc&rsquo;s flexible array strengthening.
Bit 26 is set only if the feature is supported by the compiler.  If
bit 26 is not set then bits 27 to 28 should be ignored.  Bit 27 is set
if the <samp class="option">-Wstrict-flex-arrays</samp> warnings is enabled.  Bit 28 is
set if the <samp class="option">-fstrict-flex-arrays</samp> option has been set to a
value greater than zero.
</p></dd>
</dl>

<p>The other bits are not currently used and should be set to zero so
they can be used in future extensions to the specification.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="The-CF-Encoding.html">Encoding Control Flow Protection</a>, Previous: <a href="The-PIC-Encoding.html">Encoding Position Independence</a>, Up: <a href="Examining.html">How to examine the information stored in the binary.</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
