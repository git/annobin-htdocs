<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the annobin plugin on the Fedora system.

Copyright © 2018 - 2024 Red Hat.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Built By (Annobin)</title>

<meta name="description" content="Built By (Annobin)">
<meta name="keywords" content="Built By (Annobin)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Annocheck.html" rel="up" title="Annocheck">
<link href="Hardened.html" rel="next" title="Hardened">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Built-By">
<div class="nav-panel">
<p>
Next: <a href="Hardened.html" accesskey="n" rel="next">The Hardened security checker.</a>, Up: <a href="Annocheck.html" accesskey="u" rel="up">Analysing binary files.</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<h3 class="section" id="The-builder-checker_002e"><span>4.1 The builder checker.<a class="copiable-link" href="#The-builder-checker_002e"> &para;</a></span></h3>

<div class="example smallexample">
<pre class="example-preformatted">

annocheck
  <b class="b">--builtby</b>
  <b class="b">--enable-builtby</b>
  [<b class="b">--all</b>]
  [<b class="b">--tool=</b><var class="var">name</var>]
  [<b class="b">--nottool=</b><var class="var">name</var>]
  [<b class="b">--no-version-info</b>]
  [<b class="b">--no-lang-info</b>]
  [<b class="b">--options-info</b>]
  <var class="var">file</var>...
</pre></div>


<p>The <var class="var">builtby</var> tool is disabled by default, but it can be enabled
by the command line option <samp class="option">--enable-builtby</samp> or just
<samp class="option">--builtby</samp>.  The tool checks the specified files to see if any
information is stored about how the file was built and the source
languages involved.
</p>
<p>Since the hardening checker is enabled by default it may also be
useful to add the <samp class="option">--disable-hardened</samp> option to the command
line, although this is not needed if the <samp class="option">--builtby</samp> option is
used.
</p>
<p>The tool supports a few command line options to customise its
behaviour:
</p>
<dl class="table">
<dt><code class="code">--all</code></dt>
<dd><p>Report all builder identification strings.  The tool has several
different heuristics for determining the builder.  By default it will
report the information return by the first successful heuristic.  If
the <samp class="option">--all</samp> option is enabled then all successful results will
be returned.  This will probably result in the display of multiple
instances of the same information.
</p>
</dd>
<dt><code class="code">--tool=<var class="var">name</var></code></dt>
<dd><p>This option can be used to restrict the output to only those files
which were built by a specific tool.  This can be useful when scanning
a directory full of files searching for those built by a particular
compiler.  This option can be used multiple times in order to allow a
selection of builders to be reported.
</p>
</dd>
<dt><code class="code">--nottool=<var class="var">NAME</var></code></dt>
<dd><p>This option can be used to restrict the output to only those files
which were not built by a specific tool.  This can be useful when
scanning a directory full of files searching for those that were not
built by a particular compiler.  This option can be used multiple
times in order to allow multiple builders to be hidden.
</p>
</dd>
<dt><code class="code">--lang=<var class="var">name</var></code></dt>
<dd><p>This option can be used to restrict the output to only those files
which were written in a specific high level language.  Note - not all
binaries include information about the source code language(s), so
this option may not be completely effective.  This option can be used
multiple times in order to allow a broader selection of languages to
be reported.
</p>
</dd>
<dt><code class="code">--notlang=<var class="var">NAME</var></code></dt>
<dd><p>This option can be used to restrict the output to only those files
which were not written in a specific high level language.  Note - not
all binaries include information about the source code language(s), so
this option may not be completely effective.  This option can be used
multiple times in order to allow a broader selection of languages to
be hidden.
</p>
</dd>
<dt><code class="code">--no-version-info</code></dt>
<dt><code class="code">--lang-info</code></dt>
<dd><p>By default <code class="command">builtby</code> will report the version information for
the builders that it detects.  Enabling the <samp class="option">--no-version-info</samp>
option will prevent this information from being displayed.
</p>
<p>If necessary the feature can be re-enabled by the
<samp class="option">--version-info</samp> option.
</p>
</dd>
<dt><code class="code">--no-lang-info</code></dt>
<dt><code class="code">--lang-info</code></dt>
<dd><p>By default <code class="command">builtby</code> will report the high level language(s)
of the sources used to build the program - if they have been recorded.
Enabling the <samp class="option">--no-lang-info</samp> option will prevent this
information from being displayed.
</p>
<p>If necessary the feature can be re-enabled by the
<samp class="option">--lang-info</samp> option.
</p>
</dd>
<dt><code class="code">--no-tool-info</code></dt>
<dt><code class="code">--no-builder-info</code></dt>
<dt><code class="code">--tool-info</code></dt>
<dd><p>By default <code class="command">builtby</code> will report the tool(s) used to build
the program.  Enabling the <samp class="option">--no-build-info</samp> option will
prevent this information from being displayed, meaning that only the
high level language information will shown.  Enabling this option and
the <samp class="option">--no-lang-info</samp> option effectively renders
<code class="command">built-by</code> redundant.
</p>
<p>Note <samp class="option">--no-builder-info</samp> is treated as a synonym for
<samp class="option">--no-tool-info</samp>.
</p>
<p>If necessary the feature can be re-enabled by the
<samp class="option">--tool-info</samp> option.
</p>
</dd>
<dt><code class="code">--options-info</code></dt>
<dt><code class="code">--no-options-info</code></dt>
<dd><p>By default <code class="command">builtby</code> will not report the command line options
provided to the tool(s) used to built the target binary.  Adding the
<samp class="option">--options-info</samp> option will enable the display of this
information, if it is stored in the binary (or its debug info).
</p>
<p>If necessary the feature can be disabled by the
<samp class="option">--no-options-info</samp> option. 
</p>
</dd>
</dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Hardened.html">The Hardened security checker.</a>, Up: <a href="Annocheck.html">Analysing binary files.</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
