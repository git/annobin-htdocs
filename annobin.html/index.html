<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the annobin plugin on the Fedora system.

Copyright © 2018 - 2024 Red Hat.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Top (Annobin)</title>

<meta name="description" content="Top (Annobin)">
<meta name="keywords" content="Top (Annobin)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="#Top" rel="start" title="Top">
<link href="#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Introduction.html" rel="next" title="Introduction">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
ul.toc-numbered-mark {list-style: none}
-->
</style>


</head>

<body lang="en">






<div class="top-level-extent" id="Top">
<div class="nav-panel">
<p>
Next: <a href="Introduction.html" accesskey="n" rel="next">What is Binary Annotation ?</a> &nbsp; [<a href="#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<h1 class="top" id="Annotating-Binaries_003a-How-Was-Your-Program-Built-_003f"><span>Annotating Binaries: How Was Your Program Built ?<a class="copiable-link" href="#Annotating-Binaries_003a-How-Was-Your-Program-Built-_003f"> &para;</a></span></h1>


<p>This manual describes the <small class="sc">ANNOBIN</small> plugin and the
<code class="command">annocheck</code> program, and how you can use them to determine
what security features were used when a program was built. 
</p>
<p>This manual is for <code class="code">annobin</code>
(Annobin)
version 12.0.
</p>
<p>This document is distributed under the terms of the GNU Free
Documentation License version 1.3.  A copy of the license is included
in the section entitled &ldquo;GNU Free Documentation License&rdquo;.
</p>

<div class="element-contents" id="SEC_Contents">
<h2 class="contents-heading">Table of Contents</h2>

<div class="contents">

<ul class="toc-numbered-mark">
  <li><a id="toc-What-is-Binary-Annotation-_003f" href="Introduction.html">1 What is Binary Annotation ?</a></li>
  <li><a id="toc-How-to-add-Binary-Annotations-to-your-application_002e" href="Plugins.html">2 How to add Binary Annotations to your application.</a></li>
  <li><a id="toc-How-to-examine-the-information-stored-in-the-binary_002e" href="Examining.html">3 How to examine the information stored in the binary.</a>
  <ul class="toc-numbered-mark">
    <li><a id="toc-Encoding-Protocol-and-Producer-Versions" href="The-Version-Encoding.html">3.1 Encoding Protocol and Producer Versions</a></li>
    <li><a id="toc-Encoding-Stack-Protections" href="The-STACK-Encoding.html">3.2 Encoding Stack Protections</a></li>
    <li><a id="toc-Encoding-Position-Independence" href="The-PIC-Encoding.html">3.3 Encoding Position Independence</a></li>
    <li><a id="toc-Encoding-Optimization-and-Debugging-Levels" href="The-GOW-Encoding.html">3.4 Encoding Optimization and Debugging Levels</a></li>
    <li><a id="toc-Encoding-Control-Flow-Protection" href="The-CF-Encoding.html">3.5 Encoding Control Flow Protection</a></li>
    <li><a id="toc-Encoding-the-Size-of-Enumerations" href="The-ENUM-Encoding.html">3.6 Encoding the Size of Enumerations</a></li>
    <li><a id="toc-Encoding-Instrumentation-Options" href="The-INSTRUMENT-Encoding.html">3.7 Encoding Instrumentation Options</a></li>
    <li><a id="toc-Encoding-Notes-in-a-string-format" href="String-Format-Notes.html">3.8 Encoding Notes in a string format</a></li>
  </ul></li>
  <li><a id="toc-Analysing-binary-files_002e" href="Annocheck.html">4 Analysing binary files.</a>
  <ul class="toc-numbered-mark">
    <li><a id="toc-The-builder-checker_002e" href="Built-By.html">4.1 The builder checker.</a></li>
    <li><a id="toc-The-Hardened-security-checker_002e" href="Hardened.html">4.2 The Hardened security checker.</a>
    <ul class="toc-numbered-mark">
      <li><a id="toc-The-Tests-Run-By-Annocheck" href="The-tests.html">4.2.1 The Tests Run By Annocheck</a>
      <ul class="toc-numbered-mark">
        <li><a id="toc-The-auto_002dvar_002dinit-test" href="Test-auto-var-init.html">4.2.1.1 The auto-var-init test</a></li>
        <li><a id="toc-The-bind_002dnow-test" href="Test-bind-now.html">4.2.1.2 The bind-now test</a></li>
        <li><a id="toc-The-branch_002dprotection-test" href="Test-branch-protection.html">4.2.1.3 The branch-protection test</a></li>
        <li><a id="toc-The-cf_002dprotection-test" href="Test-cf-protection.html">4.2.1.4 The cf-protection test</a></li>
        <li><a id="toc-The-dynamic_002dsegment-test" href="Test-dynamic-segment.html">4.2.1.5 The dynamic-segment test</a></li>
        <li><a id="toc-The-dynamic_002dtags-test" href="Test-dynamic-tags.html">4.2.1.6 The dynamic-tags test</a></li>
        <li><a id="toc-The-entry-test" href="Test-entry.html">4.2.1.7 The entry test</a></li>
        <li><a id="toc-The-_002dOfast-test" href="Test-fast.html">4.2.1.8 The -Ofast test</a></li>
        <li><a id="toc-The-FIPS-test" href="Test-fips.html">4.2.1.9 The FIPS test</a></li>
        <li><a id="toc-The-flex-arrays-test" href="Test-flex-arrays.html">4.2.1.10 The flex arrays test</a></li>
        <li><a id="toc-The-fortify-test" href="Test-fortify.html">4.2.1.11 The fortify test</a></li>
        <li><a id="toc-The-gaps-test" href="Test-gaps.html">4.2.1.12 The gaps test</a></li>
        <li><a id="toc-The-glibcxx_002dassertions-test" href="Test-glibcxx-assertions.html">4.2.1.13 The glibcxx-assertions test</a></li>
        <li><a id="toc-The-gnu_002drelro-test" href="Test-gnu-relro.html">4.2.1.14 The gnu-relro test</a></li>
        <li><a id="toc-The-gnu_002dstack-test" href="Test-gnu-stack.html">4.2.1.15 The gnu-stack test</a></li>
        <li><a id="toc-The-go_002drevision-test" href="Test-go-revision.html">4.2.1.16 The go-revision test</a></li>
        <li><a id="toc-The-implicit-values-test" href="Test-implicit-values.html">4.2.1.17 The implicit values test</a></li>
        <li><a id="toc-The-instrumentation-test" href="Test-instrumentation.html">4.2.1.18 The instrumentation test</a></li>
        <li><a id="toc-The-loadable-segments-test" href="Test-load-segments.html">4.2.1.19 The loadable segments test</a></li>
        <li><a id="toc-The-lto-test" href="Test-lto.html">4.2.1.20 The lto test</a></li>
        <li><a id="toc-The-not_002dbranch_002dprotection-test" href="Test-not-branch-protection.html">4.2.1.21 The not-branch-protection test</a></li>
        <li><a id="toc-The-not_002ddynamic_002dtags-test" href="Test-not-dynamic-tags.html">4.2.1.22 The not-dynamic-tags test</a></li>
        <li><a id="toc-The-notes-test" href="Test-notes.html">4.2.1.23 The notes test</a></li>
        <li><a id="toc-The-only_002dgo-test" href="Test-only-go.html">4.2.1.24 The only-go test</a></li>
        <li><a id="toc-The-openssl_002dengine-test" href="Test-OpenSSL-Engine.html">4.2.1.25 The openssl-engine test</a></li>
        <li><a id="toc-The-optimization-test" href="Test-optimization.html">4.2.1.26 The optimization test</a></li>
        <li><a id="toc-The-pic-test" href="Test-pic.html">4.2.1.27 The pic test</a></li>
        <li><a id="toc-The-pie-test" href="Test-pie.html">4.2.1.28 The pie test</a></li>
        <li><a id="toc-The-production-test" href="Test-production.html">4.2.1.29 The production test</a></li>
        <li><a id="toc-The-property_002dnote-test" href="Test-property-note.html">4.2.1.30 The property-note test</a></li>
        <li><a id="toc-The-RHIVOS-tests" href="Test-rhivos.html">4.2.1.31 The RHIVOS tests</a></li>
        <li><a id="toc-The-run_002dpath-test" href="Test-run-path.html">4.2.1.32 The run-path test</a></li>
        <li><a id="toc-The-rwx_002dseg-test" href="Test-rwx-seg.html">4.2.1.33 The rwx-seg test</a></li>
        <li><a id="toc-The-short_002denums-test" href="Test-short-enums.html">4.2.1.34 The short-enums test</a></li>
        <li><a id="toc-The-stack_002dclash-test" href="Test-stack-clash.html">4.2.1.35 The stack-clash test</a></li>
        <li><a id="toc-The-stack_002dprot-test" href="Test-stack-prot.html">4.2.1.36 The stack-prot test</a></li>
        <li><a id="toc-The-stack_002drealign-test" href="Test-stack-realign.html">4.2.1.37 The stack-realign test</a></li>
        <li><a id="toc-The-textrel-test" href="Test-textrel.html">4.2.1.38 The textrel test</a></li>
        <li><a id="toc-The-threads-test" href="Test-threads.html">4.2.1.39 The threads test</a></li>
        <li><a id="toc-The-unicode-test" href="Test-unicode.html">4.2.1.40 The unicode test</a></li>
        <li><a id="toc-The-warnings-test" href="Test-warnings.html">4.2.1.41 The warnings test</a></li>
        <li><a id="toc-The-writable_002dgot-test" href="Test-writable-got.html">4.2.1.42 The writable-got test</a></li>
        <li><a id="toc-The-zero_002dcall_002dused_002dregs-test" href="Test-zero-call-used-regs.html">4.2.1.43 The zero-call-used-regs test</a></li>
      </ul></li>
      <li><a id="toc-Command-line-options-specific-to-the-hardened-tool" href="Hardened-Command-Line-Options.html">4.2.2 Command line options specific to the hardened tool</a></li>
      <li><a id="toc-How-to-waive-the-results-of-the-hardening-tests" href="Waiving-Hardened-Results.html">4.2.3 How to waive the results of the hardening tests</a></li>
      <li><a id="toc-What-to-do-if-annocheck-reports-that-it-could-not-find-compiled-code_002e" href="Absence-of-compiled-code.html">4.2.4 What to do if annocheck reports that it could not find compiled code.</a></li>
    </ul></li>
    <li><a id="toc-The-annobin-note-displayer" href="Notes.html">4.3 The annobin note displayer</a></li>
    <li><a id="toc-The-section-size-recorder" href="Size.html">4.4 The section size recorder</a></li>
    <li><a id="toc-How-long-did-the-check-take-_003f" href="Timing.html">4.5 How long did the check take ?</a></li>
  </ul></li>
  <li><a id="toc-Allowing-other-programs-to-run-security-checks" href="Libannocheck.html">5 Allowing other programs to run security checks</a>
  <ul class="toc-numbered-mark">
    <li><a id="toc-Initialise-the-library" href="libannocheck_005finit.html">5.1 Initialise the library</a></li>
    <li><a id="toc-Close-the-library" href="libannocheck_005ffinish.html">5.2 Close the library</a></li>
    <li><a id="toc-Get-the-library-version" href="libannocheck_005fget_005fversion.html">5.3 Get the library version</a></li>
    <li><a id="toc-Convert-an-error-number-into-an-error-message" href="libannocheck_005fget_005ferror_005fmessage.html">5.4 Convert an error number into an error message</a></li>
    <li><a id="toc-Get-a-list-of-tests-supported-by-the-library" href="libannocheck_005fget_005fknown_005ftests.html">5.5 Get a list of tests supported by the library</a></li>
    <li><a id="toc-Enable-all-tests" href="libannocheck_005fenable_005fall_005ftests.html">5.6 Enable all tests</a></li>
    <li><a id="toc-Disable-all-tests" href="libannocheck_005fdisable_005fall_005ftests.html">5.7 Disable all tests</a></li>
    <li><a id="toc-Enable-a-specific-test" href="libannocheck_005fenable_005ftest.html">5.8 Enable a specific test</a></li>
    <li><a id="toc-Disable-a-specific-test" href="libannocheck_005fdisable_005ftest.html">5.9 Disable a specific test</a></li>
    <li><a id="toc-Enable-a-profile" href="libannocheck_005fenable_005fprofile.html">5.10 Enable a profile</a></li>
    <li><a id="toc-Get-a-list-of-known-profiles" href="libannocheck_005fget_005fknown_005fprofiles.html">5.11 Get a list of known profiles</a></li>
    <li><a id="toc-Run-enabled-tests" href="libannocheck_005frun_005ftests.html">5.12 Run enabled tests</a></li>
  </ul></li>
  <li><a id="toc-Configuring-annobin-and-annocheck" href="Configure-Options.html">6 Configuring annobin and annocheck</a></li>
  <li><a id="toc-How-to-use-the-information-stored-in-the-binary_002e" href="Legacy-Scripts.html">7 How to use the information stored in the binary.</a>
  <ul class="toc-numbered-mark">
    <li><a id="toc-The-built_002dby-script" href="Who-Built-Me.html">7.1 The built-by script</a></li>
    <li><a id="toc-The-check_002dabi-script" href="ABI-Checking.html">7.2 The check-abi script</a></li>
    <li><a id="toc-The-hardened-script" href="Hardening-Checks.html">7.3 The hardened script</a></li>
    <li><a id="toc-The-run_002don_002dbinaries_002din-script" href="Checking-Archives.html">7.4 The run-on-binaries-in script</a></li>
  </ul></li>
  <li><a id="toc-GNU-Free-Documentation-License" href="GNU-FDL.html">Appendix A GNU Free Documentation License</a></li>
</ul>
</div>
</div>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Introduction.html" accesskey="n" rel="next">What is Binary Annotation ?</a> &nbsp; [<a href="#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
