<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the annobin plugin on the Fedora system.

Copyright © 2018 - 2024 Red Hat.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Test cf protection (Annobin)</title>

<meta name="description" content="Test cf protection (Annobin)">
<meta name="keywords" content="Test cf protection (Annobin)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="The-tests.html" rel="up" title="The tests">
<link href="Test-dynamic-segment.html" rel="next" title="Test dynamic segment">
<link href="Test-branch-protection.html" rel="prev" title="Test branch protection">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Test-cf-protection">
<div class="nav-panel">
<p>
Next: <a href="Test-dynamic-segment.html" accesskey="n" rel="next">The dynamic-segment test</a>, Previous: <a href="Test-branch-protection.html" accesskey="p" rel="prev">The branch-protection test</a>, Up: <a href="The-tests.html" accesskey="u" rel="up">The Tests Run By Annocheck</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="The-cf_002dprotection-test"><span>4.2.1.4 The cf-protection test<a class="copiable-link" href="#The-cf_002dprotection-test"> &para;</a></span></h4>

<div class="example smallexample">
<pre class="example-preformatted">  Problem:  An attacker could compromise an unprotected binary
  Fix By:   Compiling with -fcf-protection=full
  Waive If: The application will not run on the latest Intel hardware
  Waive If: The application is built by a compiler that does not support CET
  
  Example:  FAIL: cf-protection test because only branch protection enabled
  Example:  FAIL: cf-protection test because only return protection enabled
  Example:  FAIL: cf-protection test because no protection enabled
  Example:  FAIL: cf-protection test because insufficient Control Flow sanitization
  Example:  FAIL: cf-protection test because no .note.gnu.property section = no control flow information
  Example:  FAIL: cf-protection test because CET enabling note missing
  Example:  FAIL: cf-protection test because control flow protection is not enabled
</pre></div>

<p>Intel have introduced a new security feature called <var class="var">CET</var> to their
Tiger Lake and newer cores:
</p>
<p><a class="url" href="https://www.intel.com/content/www/us/en/newsroom/opinion/intel-cet-answers-call-protect-common-malware-threats.html">https://www.intel.com/content/www/us/en/newsroom/opinion/intel-cet-answers-call-protect-common-malware-threats.html</a>
</p>
<p>This test checks to see that this feature is enabled.  Normally this
is done by compiling the code with the <samp class="option">-fcf-protection</samp> or
<samp class="option">-fcf-protection=full</samp> command line option enabled.  (The first
form of the option is an alias for the second, but the second form is
preferred as it explicitly shows that all of the control flow
protection features are being enabled).
</p>
<p>But if an application contains assembler code, or it is linked against
a library that has not been built with the protection enabled, or it
is built by a compiler that does not support <var class="var">CET</var> then this test
can fail.
</p>
<p>The feature has to be enabled in the compiler as it involves inserting
new instructions into the compiled code.  The feature is also an
all-or-nothing type proposition for any process.  Either all of the
code in the process must have been built to support CET - in which
case the feature can be enabled - or if even a single component does
not support CET then it must be disabled for the entire process.
</p>
<p>In order to enforce this the compiler inserts a special note into
compiled object files (the .note.gnu.property section referred to
above).  The note indicates that CET is supported, as well as details
of the minimum x86 architecture revision needed and so on.
</p>
<p>Then when the object files are linked together to create the
executable the linker checks all of these notes, and if any object
file or library is missing one then it does not put a note in
the output executable.  Alternatively if all of the object files (and
libraries of course) do have notes, but one or more of them do not
have the CET-is-enabled flag, then the linker copies the notes into
the executable, but always clears the CET-is-enabled flag.
</p>
<p>Finally when a program is executed the run-time loader checks this note
and if the CET-is-enabled flag is present then it enables the CET
feature in the hardware.
</p>
<p>Fixing this check either means enabling the
<samp class="option">-fcf-protection=full</samp> (for gcc) or the
<samp class="option">-fcf-protection-branch</samp> and <samp class="option">-fcf-protection-return</samp>
options (for Clang).
</p>
<p>If an assembler source file is used as part of an application then it
too needs to be updated.  Any location in the source code where an
indirect branch or function call can land must now have either
<var class="var">ENDBR64</var> (for 64-bit assembler) or <var class="var">ENDBR32</var> (for 32-bit
assembler) as the first instruction executed.
</p>
<p>In addition the assembler needs a note to indicate that it now supports
CET.  This note can be added via including this code snippet in the
sources: 
</p>
<div class="example smallexample">
<pre class="example-preformatted">	.section	.note.gnu.property,&quot;a&quot;
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 &quot;GNU&quot;
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
</pre></div>

<p>If necessary the test can be disabled via the <samp class="option">--skip-cf-protection</samp>
option and re-enabled via the <samp class="option">--test-cf-protection</samp> option.
</p>
<p>For more information on CET see: 
<a class="url" href="https://www.intel.com/content/dam/develop/external/us/en/documents/catc17-introduction-intel-cet-844137.pdf">https://www.intel.com/content/dam/develop/external/us/en/documents/catc17-introduction-intel-cet-844137.pdf</a>
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Test-dynamic-segment.html">The dynamic-segment test</a>, Previous: <a href="Test-branch-protection.html">The branch-protection test</a>, Up: <a href="The-tests.html">The Tests Run By Annocheck</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
