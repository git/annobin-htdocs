<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the annobin plugin on the Fedora system.

Copyright © 2018 - 2024 Red Hat.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Annocheck (Annobin)</title>

<meta name="description" content="Annocheck (Annobin)">
<meta name="keywords" content="Annocheck (Annobin)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="Libannocheck.html" rel="next" title="Libannocheck">
<link href="Examining.html" rel="prev" title="Examining">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="chapter-level-extent" id="Annocheck">
<div class="nav-panel">
<p>
Next: <a href="Libannocheck.html" accesskey="n" rel="next">Allowing other programs to run security checks</a>, Previous: <a href="Examining.html" accesskey="p" rel="prev">How to examine the information stored in the binary.</a>, Up: <a href="index.html" accesskey="u" rel="up">Annotating Binaries: How Was Your Program Built ?</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<h2 class="chapter" id="Analysing-binary-files_002e"><span>4 Analysing binary files.<a class="copiable-link" href="#Analysing-binary-files_002e"> &para;</a></span></h2>


<div class="example smallexample">
<pre class="example-preformatted">annocheck
  [<b class="b">-h | --help</b>]
  [<b class="b">--help-<var class="var">tool</var></b>]
  [<b class="b">--version</b>]
  [<b class="b">-v | --verbose</b>]
  [<b class="b">-q | --quiet</b>]
  [<b class="b">-i | --ignore-unknown</b>]
  [<b class="b">-r | --report-unknown</b>]
  [<b class="b">-f | --follow-links</b>]
  [<b class="b">-I | --ignore-links</b>]
  [<b class="b">--debug-rpm=</b><var class="var">file</var>]
  [<b class="b">--debug-file=</b><var class="var">file</var>]
  [<b class="b">--debug-dir=</b><var class="var">dir</var>]
  [<b class="b">-p <var class="var">text</var> | --prefix=</b><var class="var">text</var>]
  [<b class="b">-t <var class="var">dir</var> | --tmpdir=</b><var class="var">dir</var>]
  [<b class="b">-u | --use-debuginfod</b>]
  [<b class="b">-n | --no-use-debuginfod</b>]
  [<b class="b">--enable-<var class="var">tool</var></b>]
  [<b class="b">--disable-<var class="var">tool</var></b>]
  [<b class="b">--<var class="var">tool</var></b>]
  [<b class="b">--<var class="var">tool</var>-<var class="var">option</var></b>]
  <var class="var">file</var>...
</pre></div>


<p>The <code class="command">annocheck</code> program can analyse binary files and report
information about them.  It is designed to be modular, with a set of
self-contained tools providing the checking functionality.
Currently the following tools are implemented:
</p>

<p>The <code class="command">annocheck</code> program is able to scan inside rpm files and
libraries.  It will automatically recurse into any directories that
are specified on the command line.  In addition <code class="command">annocheck</code>
knows how to find debug information held in separate debug files, and
it will search for these whenever it needs the resources that they
contain.
</p>
<p>New tools can be added to the annocheck framework by creating a new
source file and including it in the <samp class="file">Makefile</samp> used to build
<code class="command">annocheck</code>.  The modular nature of <code class="command">annocheck</code> means
that nothing else needs to be updated.
</p>
<p>New tools must fill out a <code class="code">struct checker</code> structure (defined in
<samp class="file">annocheck.h</samp>) and they must define a constructor function that
calls <code class="code">annocheck_add_checker</code> to register their presence at
program start-up.
</p>
<p>The <code class="command">annocheck</code> program supports some generic command line
options that are used regardless of which tools are enabled.
</p>
<dl class="table">
<dt><code class="code">--debug-rpm=<var class="var">file</var></code></dt>
<dd><p>Look inside <var class="var">file</var> for separate dwarf debug information.
Multiple instances of the <samp class="option">--debug-rpm</samp> option accumulate.
</p>
<p>Note - if none of the <samp class="option">--debug-rpm</samp>, <samp class="option">--debug-file</samp> and
<samp class="option">--debug-dir</samp> options are used and a single source file is
specified to be scanned, and this file&rsquo;s name ends in <em class="emph">.rpm</em> then
annocheck will automatically look to see if it can find an associated
debug info rpm based upon the filename.
</p>
</dd>
<dt><code class="code">--debug-file=<var class="var">file</var></code></dt>
<dd><p>Look in <var class="var">file</var> for dwarf debug information.
Multiple instances of this option accumulate.
</p>
</dd>
<dt><code class="code">--debug-dir=<var class="var">dir</var></code></dt>
<dd><p>Look in directory <var class="var">dir</var> for separate dwarf debug information files.
Note: multiple instances of this option do <em class="emph">not</em> accumulate.
</p>
</dd>
<dt><code class="code">--help</code></dt>
<dt><code class="code">-h</code></dt>
<dd><p>Displays the generic annobin usage information and then exits.
</p>
</dd>
<dt><code class="code">--help-<var class="var">tool</var></code></dt>
<dd><p>Display the usage information for <var class="var">tool</var> and then exits.
</p>
</dd>
<dt><code class="code">--report-unknown</code></dt>
<dt><code class="code">--ignore-unknown</code></dt>
<dt><code class="code">-r</code></dt>
<dt><code class="code">-i</code></dt>
<dd><p>If enabled, unknown file types are reported when they are encountered.
This includes non-ELF format files, block devices and so on.
Directories are not considered to be unknown and are automatically
descended.
</p>
<p>The default setting depends upon the file being processed.  For rpm
files the default is to ignore unknowns, since these often contain
non-executable files.  For other file types, including directories,
the default is to report unknown files.
</p>
</dd>
<dt><code class="code">--ignore-links</code></dt>
<dt><code class="code">--follow-links</code></dt>
<dt><code class="code">-I</code></dt>
<dt><code class="code">-f</code></dt>
<dd><p>Specifies whether symbolic links should be followed or ignored.
</p>
<p>The default setting depends upon the file being processed.  For rpm
files the default is to ignore symbolic links, since these often
unresolveable.  For other file types, including directories,
the default is to follow the links.
</p>
</dd>
<dt><code class="code">--prefix=<var class="var">text</var></code></dt>
<dt><code class="code">-p <var class="var">text</var></code></dt>
<dd><p>Include <var class="var">text</var> in the output description.
</p>
</dd>
<dt><code class="code">--quiet</code></dt>
<dt><code class="code">-q</code></dt>
<dd><p>Do not print anything, just return an exit status.
</p>
</dd>
<dt><code class="code">--tmpdir=<var class="var">dir</var></code></dt>
<dt><code class="code">-t <var class="var">dir</var></code></dt>
<dd><p>Use <var class="var">dir</var> as a directory for holding temporary files.
</p>
</dd>
<dt><code class="code">--verbose</code></dt>
<dt><code class="code">-v</code></dt>
<dd><p>Produce informational messages whilst working.  Repeat for more
information.
</p>
</dd>
<dt><code class="code">--version</code></dt>
<dd><p>Report the version of the tool and then exit.
</p>
</dd>
<dt><code class="code">--use-debuginfod</code></dt>
<dt><code class="code">-u</code></dt>
<dd><p>Enable the use of the debuginfod service to download debuginfo rpms.
This feature is enabled by default, but it is only active if support
for the debuginfod server has been compiled in to annocheck.
</p>
</dd>
<dt><code class="code">--no-use-debuginfod</code></dt>
<dt><code class="code">-n</code></dt>
<dd><p>Do not use the debuginfod service, even if it is available.
</p>
</dd>
<dt><code class="code">--enable-<var class="var">tool</var></code></dt>
<dd><p>Enable <var class="var">tool</var>.  Most tools are disabled by default and so need to
be enabled via this option before they will act.
</p>
</dd>
<dt><code class="code">--disable-<var class="var">tool</var></code></dt>
<dd><p>Disable <var class="var">tool</var>.  Normally used to disable the hardening checker,
which is enabled by default.
</p>
</dd>
<dt><code class="code">--<var class="var">tool</var></code></dt>
<dd><p>Enable <var class="var">tool</var> and disable all other tools.
</p>
</dd>
<dt><code class="code">--<var class="var">tool</var>-<var class="var">option</var></code></dt>
<dd><p>Pass <var class="var">option</var> on to <var class="var">tool</var>.
</p>
</dd>
</dl>

<p>Any other command line options will be passed to the tools in turn in
order to give them a chance to claim and process them.
</p>

<ul class="mini-toc">
<li><a href="Built-By.html" accesskey="1">The builder checker.</a></li>
<li><a href="Hardened.html" accesskey="2">The Hardened security checker.</a></li>
<li><a href="Notes.html" accesskey="3">The annobin note displayer</a></li>
<li><a href="Size.html" accesskey="4">The section size recorder</a></li>
<li><a href="Timing.html" accesskey="5">How long did the check take ?</a></li>
</ul>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Libannocheck.html">Allowing other programs to run security checks</a>, Previous: <a href="Examining.html">How to examine the information stored in the binary.</a>, Up: <a href="index.html">Annotating Binaries: How Was Your Program Built ?</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
