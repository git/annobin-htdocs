<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the annobin plugin on the Fedora system.

Copyright © 2018 - 2024 Red Hat.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Test dynamic tags (Annobin)</title>

<meta name="description" content="Test dynamic tags (Annobin)">
<meta name="keywords" content="Test dynamic tags (Annobin)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="The-tests.html" rel="up" title="The tests">
<link href="Test-entry.html" rel="next" title="Test entry">
<link href="Test-dynamic-segment.html" rel="prev" title="Test dynamic segment">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Test-dynamic-tags">
<div class="nav-panel">
<p>
Next: <a href="Test-entry.html" accesskey="n" rel="next">The entry test</a>, Previous: <a href="Test-dynamic-segment.html" accesskey="p" rel="prev">The dynamic-segment test</a>, Up: <a href="The-tests.html" accesskey="u" rel="up">The Tests Run By Annocheck</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="The-dynamic_002dtags-test"><span>4.2.1.6 The dynamic-tags test<a class="copiable-link" href="#The-dynamic_002dtags-test"> &para;</a></span></h4>

<div class="example smallexample">
<pre class="example-preformatted">  Problem:  Unprotected AArch64 binaries are vulnerable to ROP/JOP style attacks
  Fix By:   Compile with -mbranch-protection=standard
  Waive If: Not running on AArch64
  Waive If: The application will not run on Fedora 35 or later.
  Waive If: The application will not run on newer AArch64 cores.

  Example:  FAIL: dynamic tags test because BTI_PLT and PAC_PLT flags missing from the dynamic tags
  Example:  FAIL: dynamic tags test because BTI_PLT flag is missing from the dynamic tags
  Example:  FAIL: dynamic tags test because PAC_PLT flag is missing from the dynamic tags
  Example:  FAIL: dynamic tags test because no dynamic tags found
</pre></div>

<p>AArch64 processors are vulnerable to a class of attack known as
<var class="var">ROP</var> and <var class="var">JOP</var> style attacks.  Preventing this kind of
exploit requires assistance from the hardware itself, in the form of
new instructions that need to be inserted by the compiler, and new
bits in the core&rsquo;s status that need to be set.
</p>
<p>This test checks to see if executable binaries have been marked as
supporting the necessary security features to prevent this kind of
attack.  (The BTI_PLT and PAC_PLT flags mentioned in the failure
messages).  If they are marked then the runtime loader can enable the
features in the processor core.  This marking is done by setting flags
in the tags found in the dynamic section of the executable.  If the
flags are missing then the executable is considered to be unprotected.
</p>
<p>The security features are enabled by compiling with the
<samp class="option">-mbranch-protection=standard</samp> gcc command line option.
</p>
<p>Note - these security features are only found on newer versions of the
AArch64 architecture, and they need a compiler and a loader that will
support them.  Currently this means Fedora 35 or later, but not RHEL-8
or RHEL-9.
</p>
<p>If an assembler source file is used as part of an application then it
too needs to be updated.  Any location in the source code where an
indirect branch or function call can land must now have <var class="var">BTI</var> 
as the first instruction executed.  This instruction is safe to use
even in code that will not be executed in a BTI-enabled environment as
it translates into a no-op instruction if not needed.
</p>
<p>In addition the assembler needs a note to indicate that it now supports
BTI.  This note can be added via including this code snippet in the
sources: 
</p>
<div class="example smallexample">
<pre class="example-preformatted">	.pushsection	.note.gnu.property, &quot;a&quot;
	.align	3
	.word	2f - 1f
	.word	4f - 3f
	.word	5            /* NT_GNU_PROPERTY_TYPE_0 */
1:	.asciz	&quot;GNU&quot;

2:	.align	3
3:	.word	0xc0000000   /* type: GNU_PROPERTY_AARCH64_FEATURE_1_AND */
	.word	6f - 5f	     /* size */
5:	.word	1            /* value: GNU_PROPERTY_AARCH64_FEATURE_1_BTI */

6:	.align	3
4:	.popsection
</pre></div>

<p>Note - this test is the inverse of the <a class="ref" href="Test-not-dynamic-tags.html">The not-dynamic-tags test</a>
test and directly related to the <a class="ref" href="Test-branch-protection.html">The branch-protection test</a> test.
</p>
<p>Note - this test is automatically enabled if one of the following
profile options is used:
</p>
<dl class="table">
<dt><code class="code"><samp class="option">--profile=rawhide</samp></code></dt>
<dt><code class="code"><samp class="option">--profile=f43</samp></code></dt>
<dt><code class="code"><samp class="option">--profile=f42</samp></code></dt>
<dt><code class="code"><samp class="option">--profile=f41</samp></code></dt>
<dt><code class="code"><samp class="option">--profile=f40</samp></code></dt>
<dt><code class="code"><samp class="option">--profile=f39</samp></code></dt>
<dt><code class="code"><samp class="option">--profile=f38</samp></code></dt>
<dt><code class="code"><samp class="option">--profile=f37</samp></code></dt>
<dt><code class="code"><samp class="option">--profile=f36</samp></code></dt>
<dt><code class="code"><samp class="option">--profile=el10</samp></code></dt>
</dl>

<p>The test is automatically disabled if one of the other profile options
is used, ie:
</p>
<dl class="table">
<dt><code class="code"><samp class="option">--profile=el7</samp></code></dt>
<dt><code class="code"><samp class="option">--profile=el8</samp></code></dt>
<dt><code class="code"><samp class="option">--profile=el9</samp></code></dt>
<dt><code class="code"><samp class="option">--profile=f35</samp></code></dt>
</dl>

<p>If necessary the test can be disabled via the <samp class="option">--skip-dynamic-tags</samp>
option and re-enabled via the <samp class="option">--test-dynamic-tags</samp> option.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Test-entry.html">The entry test</a>, Previous: <a href="Test-dynamic-segment.html">The dynamic-segment test</a>, Up: <a href="The-tests.html">The Tests Run By Annocheck</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
